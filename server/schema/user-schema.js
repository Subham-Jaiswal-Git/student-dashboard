import mongoose from "mongoose";
import autoIncrement from 'mongoose-auto-increment';

const userSchema = mongoose.Schema({
    cname:String,
    mobile:String,
    mail:String,
    dob:Date,
    gender:String,
    rel:String,
    address:String,
    adate:Date,
    class:String,
    roll:String
});

autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, 'cms');

const user = mongoose.model('cms', userSchema);

export default user;